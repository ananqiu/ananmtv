﻿#!/bin/bash

#获取工作路径
CURRENT_DIR=$(
	cd $(dirname $0)
	pwd
)
#读取token
token=$(cat $CURRENT_DIR/bb.json)
#设置默认Headers参数
Header="User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.54 Safari/537.36"
Rererer=https://www.aliyundrive.com/
#刷新token并获取drive_id、access_token
response=$(curl https://auth.aliyundrive.com/v2/account/token -X POST -H "User-Agent:$Header" -H "Content-Type:application/json" -H "Rererer:$Rererer" -d '{"refresh_token":"'$token'", "grant_type": "refresh_token"}')
drive_id=$(echo "$response" | sed -n 's/.*"default_drive_id":"\([^"]*\).*/\1/p')
new_token=$(echo "$response" | sed -n 's/.*"refresh_token":"\([^"]*\).*/\1/p')
access_token=$(echo "$response" | sed -n 's/.*"access_token":"\([^"]*\).*/\1/p')
if [ -z "$new_token" ]; then
	echo "刷新Token失败"
	exit 1
fi
echo -n ${new_token} >$CURRENT_DIR/bb.json
echo -n "{
"use_internal_storage": false,
"token": "a30c9537ec124510ace915ba78c0374d",
"open_token": "",
"open_api_url": "get|https://adrive.xdow.net/oauth/access_token",
"oauth_client_id": "",
"oauth_client_secret": "",
"oauth_auth_url": "",
"oauth_refresh_url": "",
"thread_limit": 0,
"is_vip": true,
"vip_thread_limit": 0,
"vod_flags": "4kz|auto",
"quark_thread_limit": 10,
"quark_is_vip": true,
"quark_vip_thread_limit": 10,
"quark_flags": "4kz|auto",
"uc_thread_limit": 0,
"uc_is_vip": false,
"uc_vip_thread_limit": 0,
"uc_flags": "4kz|auto",
"thunder_thread_limit": 2,
"thunder_is_vip": false,
"thunder_vip_thread_limit": 2,
"thunder_flags": "4k|4kz|auto",
"aliproxy": "",
"aliproxy_url": "https://d.kstore.space/download/7395/aliproxy.gz",
"proxy": "",
"danmu": true,
"quark_danmu": true,
"quark_cookie": "_UP_A4A_11_=wb9001e0d3254cde8501d306f653ad4f; _UP_335_2B_=1; _UP_30C_6A_=st9616201be5nvg14z3frfq3vovaxxnc; _UP_TS_=sg11e11c6833e847f7a002e409f932e242d; _UP_E37_B7_=sg11e11c6833e847f7a002e409f932e242d; _UP_TG_=st9616201be5nvg14z3frfq3vovaxxnc; _UP_F7E_8D_=pqqDQAn1xrc6ds179jeK1kPwKLOVbxJPcg0RzQPI6KmBtV6ZMgPh38l93pgubgHDQqhaZ2Sfc0qv%2BRantbfg1mWGAUpRMP4RqXP78Wvu%2FCfvkWWGc5NhCTV71tGOIGgDBR3%2Bu6%2Fjj46CHerxD%2BCvCEK5g9ZXuOTkvOTidzNw8s%2FWtKAIxWbnCzZn4%2FJMBUubWxICea0HlQgYBUdWb5J5%2B956Z0ZQBoxZNxeKBkgw%2Bh6U9eShnMesDRU4wR0Pq7NklczEGdRq2nIAcu7v22Uw2o%2FxMY0xBdeC9Korm5%2FNHnxl6K%2Bd6FXSoT9a3XIMQO359auZPiZWzrNlZe%2BqnOahXcx7KAhQIRqSOapSmL4ygJor4r5isJhRuDoXy7vJAVuH%2FRDtEJJ8rZTq0BdC23Bz%2B0MrsdgbK%2BiW; _UP_D_=pc; __pus=7775ccf71b7676fb32b49b58a5b9996fAATcIKUiIxLsnvd9qAbN8nnzTrhXkOaLBTbxZQPI4X9kGRTOBaG1ekVjwv9IyzcwFQT5kjWtbRn8P8fknIGjuHlV; __kp=ebd3db90-d982-11ee-b896-cbd68dcfed5c; __kps=AARAQQ3cvkAMwV4qyMoLLj45; __ktd=8bv39XeosiuLWJTTCwMUOg==; __uid=AARAQQ3cvkAMwV4qyMoLLj45; __puus=cf001985b8c81815c17301e83a006a82AAS3rkORtUUaPhKA5NecHEGhzBdmDGY2BuavaTA5v9JUFgsJK3DXwCH2Jaa84xdgWdG+xpON26RiZqHOlQn6YAQYXsIqnQ4savh0vt4mbcMa1FeoK3dWM2uFfjuabayIqujUXJXJ6vZPx1itpEEZTejp63evNuatCW4cm2ljWveFdhQsAgKVt1oLqyWc2VJZ2MNL4g9aN70RZVevrEsclHhQ",
"uc_cookie": "",
"thunder_username": "+8619370213753",
"thunder_password": "_Hh1860111",
"thunder_captchatoken": "captcha_token=ck0.ASSZcrpDYWgsPZW5mXbsaiP6Wjf3p_7KBQuuoZRvAzB_gpbx2lO5QWdYJLtWBtqZ4EB1dQZFBfwYVkG0P8LNHyw08mm7qfRaCkazGJ93HCSpwcK8cvf9C93T5Dk9L3L7vGc8LO1dPaS1n3Rn5uXVXQ-lgr47Ppui7XxIZvGPyTxO-Fj2KT1WfIsFX92XppWCG5o6UgIqdmsLpGy5GM3eI0fGkUbwHEa0DajLsDFJpr8CrRr2Y8hbvx2lI95LEe9J_vdKYONKZ836rjqiUkvnBVvAxosHwlmfABXpelQb7Oag8V_h1EBSywo5H62zFI2OHRH7CXzV7rQ3rQoXIhpuycmzg1pBCQkhohw3NGxJ7Yg",
"yd_auth": "Basic bW9iaWxlOjE4MDE2NTAzMTMyOngwT3BOMWdofDF8UkNTfDE3MTM1ODI4MTQ0NjF8QWp1cUdtZFZUeHhYSzkyODRYQk1JTjlFM0ZqLkVDRnhGZDR5WEtQU05UdlFyaVpXRjFtRzU5ZC5yN0tRam1US0hRUG41bk8ydHl2NjBqWjl4UG51clp1aTVhVkk2MjBkRUJxbGdyU1JNQUtFX2l3V1REVE5JXzA2eksyX3oubGFncFBiMjZJYWVPSy5iM0NQd0xsbTJfQmNucXRTQUxuUnNldXhTOFkuNlpRLQ==",
"yd_thread_limit":4,
"yd_flags": "auto|4kz",
"yd_danmu": true,
"pikpak_username":"dorabohan@lista.cc",
"pikpak_password":"aa123456",
"pikpak_flags": "4k|auto",
"pikpak_thread_limit": 2,
"pikpak_vip_thread_limit": 2,
"pikpak_proxy":"socks5://127.0.0.1:10072",
"pikpak_proxy_onlyapi":true,
"pikpak_danmu":true,
"wgcf_key":"eLPYOt5B+H9sQQrg6tbx13pPt3I5B/lo/LfP/NcU83s=",
"wgcf_key2":"bmXOC+F1FxEMF9dyiK2H5/1SUtzH0JuVo51h2wPfgyo=",
"wgcf_ipport":"162.159.192.131:878",
"wgcf_xray_url":"./xray.gz",
"wgcf_json_url":"./wgcf.json",
"wgcf_vless_id":"5008aa6b-004d-477a-9a6b-724df8e2f28a",
"wgcf_vless_optname":"112.3.30.167:80",
"wgcf_vless_worker":"tms.dingtalk.com",
"wgcf_vless_path":"/",
"wgcf_vless_protocol":"vmess",
"wgcf_vless_network":"ws",
"wgcf_vless_tls":false
}
">$CURRENT_DIR/tokenm.json

#签到
response=$(curl "https://member.aliyundrive.com/v1/activity/sign_in_list" -X POST -H "User-Agent:$Header" -H "Content-Type:application/json" -H "Authorization:Bearer $access_token" -d '{"grant_type":"refresh_token", "refresh_token":"'$new_token'"}')
success=$(echo $response | cut -f1 -d, | cut -f2 -d:)
if [ $success = "true" ]; then
	echo "阿里签到成功"
fi
signday=$(echo "$response" | grep -o '"day":[0-9]\+' | cut -d':' -f2-)
status=$(echo "$response" | grep -o "\"status\":\"[^\"]*\"" | cut -d':' -f2- | tr -d '"')
isReward=$(echo "$response" | grep -o '"isReward":[^,}]\+' | cut -d':' -f2- | sed '1d')
col=1
for day in $signday; do
	stat=$(echo $status | awk -v col="$col" '{print $col}')
	reward=$(echo $isReward | awk -v col="$col" '{print $col}')
	if [ $stat = "normal" ] && [ "$reward" = "false" ]; then
		echo "正在领取第$day天奖励"
		curl -s -H "User-Agent:$Header" -H "Authorization:Bearer $access_token" -H "Content-Type: application/json" -X POST -d '{"refresh_token":"'$token'", "grant_type": "refresh_token", "signInDay": "'$day'"}' "https://member.aliyundrive.com/v1/activity/sign_in_reward"
	fi
	col=$((col + 1))
done

#删除文件
delete_File() {
	response=$(curl -s -H "User-Agent:$Header" -H "Authorization:Bearer $access_token" -H "Content-Type: application/json" -X POST -d '{"drive_id": "'$drive_id'","parent_file_id": "root"}' "https://api.aliyundrive.com/adrive/v3/file/list")
	if [ -z "$(echo "$response" | grep "items")" ]; then
		echo "获取文件列表失败"
		exit 1
	fi
	file_lines=$(echo "$response" | grep -o "\"file_id\":\"[^\"]*\"" | cut -d':' -f2- | tr -d '"')
	type_lines=$(echo "$response" | grep -o "\"type\":\"[^\"]*\"" | cut -d':' -f2- | tr -d '"')
	col=1
	for file_id in $file_lines; do
		type=$(echo $type_lines | awk -v col="$col" '{print $col}')
		if [ $type = "file" ]; then
			curl -s -H "User-Agent:$Header" -H "Authorization:Bearer $access_token" -H "Content-Type: application/json" -X POST -d '{"requests": [{"body": {"drive_id": "'$drive_id'", "file_id": "'$file_id'"}, "headers": {"Content-Type": "application/json"}, "id": "'$file_id'", "method": "POST", "url": "/file/delete"}], "resource": "file"}' "https://api.aliyundrive.com/v3/batch"
		fi
		col=$((col + 1))
	done
}

INPUT=$1
case $INPUT in
delete_File) delete_File ;;
esac

